import {Provider} from "react-redux";
import Cinema from "./containers/Cinema";
import {applyMiddleware, combineReducers} from "redux";
import {reducer} from "./reducer/cinema";
import * as Redux from "redux";
import withAuthorities from "../../decorators/withAuthorities";
import React from "react";
import thunkMiddleware from "redux-thunk";


const resultReducer = combineReducers({
    cinema : reducer
});


const store = Redux.createStore(resultReducer
,applyMiddleware(thunkMiddleware),);



export default withAuthorities(props => (
    <Provider store={store}>
        <Cinema {...props} />
    </Provider>
));