export const fetchData = () => async dispatch => {
    dispatch({ type: 'FETCH_DATA_REQUEST' });
    try {
        const response = await fetch('http://localhost:8080/screen/findAll');
        const data = await response.json();
        dispatch({ type: 'FETCH_DATA_SUCCESS', payload: data });
    } catch (error) {
        dispatch({ type: 'FETCH_DATA_FAILURE', payload: error.message });
    }
};
export const deleteItem = id => async dispatch => {
    dispatch({ type: 'DELETE_DATA_REQUEST' });
    try {
        await fetch(`http://localhost:8080/screen/delete/${id}`, { method: 'DELETE' });
        dispatch({ type: 'DELETE_DATA_SUCCESS', payload: id });
    } catch (error) {
        dispatch({ type: 'DELETE_DATA_FAILURE', payload: error.message });
    }
};