const initialState = {
    data: [],
    loading: false,
    error: false,
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_DATA_REQUEST':
            return { ...state, loading: true };

        case 'FETCH_DATA_SUCCESS':
            return { ...state, loading: false, data: action.payload };
        case 'FETCH_DATA_FAILURE':
            return { ...state, loading: false, error: action.payload };
        case 'DELETE_ITEM_REQUEST':
            return { ...state, loading: true };
        case 'DELETE_ITEM_SUCCESS':
            return { ...state, loading: false, items: state.items.filter(item => item.id !== action.payload) };
        case 'DELETE_ITEM_FAILURE':
            return { ...state, loading: false, error: action.payload };
        default:
            return state;
    }
};