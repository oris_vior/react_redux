import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {deleteItem, fetchData} from '../action/cinema';
import '../style.css';
import {Button} from "@material-ui/core";


function Cinema(props) {
    const data = useSelector(state => state);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchData())
    }, [dispatch])

    const handleDelete = id => {
        dispatch(deleteItem(id));
        window.location.reload();
    };


    return (
        <div>
            {data.cinema.data.map((value, key) =>
                <div
                    className="my-component"
                    key={key}>{value.id} {value.model} {value.cinema.name}
                    <div className="buttons">
                        <Button>Update</Button>
                        <Button onClick={() => handleDelete(value.id)}>Delete</Button>
                    </div>

                </div>)}
        </div>);

}

export default Cinema;