import React from 'react';
import PageContainer from "../components/PageContainer";
import PageAccessValidator from "../components/PageAccessValidator";
import CinemaPage from 'pages/Cinema';

const Cinema = () => (
    <PageAccessValidator>
        <PageContainer>
            <CinemaPage />
        </PageContainer>
    </PageAccessValidator>
);

export default Cinema;